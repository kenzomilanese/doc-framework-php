# Doc 1

# Séance 1:

# Installation VM Centos:

# Installation des prérequis:

    #!/bin/bash
    
    yum --enablerepo=remi,epel install httpd
    
    yum --enablerepo=remi,epel install mysql-server
    
    service mysqld start
    
    /usr/bin/mysql_secure_installation
    
    yum --enablerepo=remi,epel install php php-zip php-mysql php-mcrypt php-xml php-mbstring
    
    service httpd restart
    
    curl -sS https://getcomposer.org/installer | php
    
    mv composer.phar /usr/bin/composer
    
    chmod +x /usr/bin/composer
    
    cd ~/var/www
    
    git clone https://github.com/laravel/laravel.git
    
    cd
    
    cd ~/var/www/laravel
    
    composer install
    
    cd
    
    chown -R apache.apache /var/www/laravel
    
    chmod -R 755 /var/www/laravel
    
    chmod -R 755 /var/www/laravel/storage
    
    chcon -R -t httpd_sys_rw_content_t /var/www/laravel/storage
    
    cp .env.example .env
    
    php artisan key:generate



emacs /etc/httpd/conf/httpd.conf

création page de test.

service httpd restart

# Authentification:

composer require laravel/ui --dev

php artisan ui vue --auth

npm install && npm run dev


# Seed Database:

php artisan make:seeder UsersTableSeeder

Créer une boucle tant que i < 100 ajouter un user à la bdd.

# Fichier database seeder:

    class DatabaseSeeder extends Seeder
    {
        /**
        * Run the database seeds.
        *
        * @return void
        */
        public function run()
        {
            $i = 0;

            while (i <= 100) {
            DB::table('users')->insert([
                'name' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => bcrypt('password'),
            ]);
            i++;
            }
        }
    }

composer dump-autoload

php artisan db:seed

Activation virtualisation dans le BIOS.

Installation vagrant, virtualbox, git et Homestead.

git clone https://github.com/laravel/homestead.git ~/Homestead

CMD->Dossier homestead->vagrant box add laravel/homestead --force

bash init.sh

ssh-keygen -t rsa -C 'moncourriel@mondomaine.com'

Modification des chemins d'accès Laravel.

Modification fichiers hosts

Configuration pour accès mysql a distance via Navicat (GRANT PRIVILEGES)


# Seance 2:

# Installation de spatie permission:

composer require spatie/laravel-permission

composer require spatie/laravel-permission

php artisan migrate

php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="config"

# Création des roles:
Création de 3 roles (user, moderator et admin).

# Exemple de création de role :

$role = Role::create(['name' => 'user']);

# Seed Database avec des roles :

    class DatabaseSeeder extends Seeder
    {
        /**
        * Run the database seeds.
        *
        * @return void
        */
        public function run()
        {
            $i = 0;
            $id = 1806; // Numéro du 1er ID actuel dans la db.

            while ($i <= 100) {
                if ($i <= 10) {
                    DB::table('model_has_roles')->insert([
                    'role_id' => '3',
                    'model_type' => 'App\User',
                    'model_id' => $id,
                    ]);
                }
                if ($i <= 50 && $i > 10) {
                    DB::table('model_has_roles')->insert([
                    'role_id' => '2',
                    'model_type' => 'App\User',
                    'model_id' => $id,
                ]);
                }
                if ($i <= 100 && $i > 50) {
                    DB::table('model_has_roles')->insert([
                    'role_id' => '1',
                    'model_type' => 'App\User',
                    'model_id' => $id,
                ]);
                    }
                    $i++;
                    $id++;
                }
            }
    }

# Utilisation des Middlewares

# Mapping des routes dans le RouteServiceProvider :

    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapAdminRoutes();
        $this->mapModoRoutes();
        $this->mapUserRoutes();

        //
    }
    protected function mapAdminRoutes()
    {
        Route::middleware('admin')
             ->namespace($this->namespace)
             ->group(base_path('routes/admin.php'));
    }
    protected function mapModoRoutes()
    {
        Route::middleware('moderator')
             ->namespace($this->namespace)
             ->group(base_path('routes/moderator.php'));
    }
    protected function mapUserRoutes()
    {
        Route::middleware('writer')
             ->namespace($this->namespace)
             ->group(base_path('routes/user.php'));
    }

# Création du fichier php route :

# Exemple pour le moderator.php

    <?php

    Route::get('moderator/dashboard', function(){
        return 'Welcome Moderator!';
    })->name('moderator.dashboard');

# Création des Middlewares



# Modification du Kernel.php



    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'role' => \Spatie\Permission\Middlewares\RoleMiddleware::class,
        'permission' => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
    ];

    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
        'admin' => [
            'web',
            'auth',
            'role:admin'
        ],
        'moderator' => [
            'web',
            'auth',
            'role:moderator'
        ],
        'writer' => [
            'web',
            'auth',
            'role:writer'
        ],
        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

# Séance 4


php artisan make:model Contact --migration

# Modification de la migration :

	<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateContactsTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('contacts', function (Blueprint $table) {
				$table->increments('id');
				$table->timestamps();
				$table->string('first_name');
				$table->string('last_name');
				$table->string('email');
				$table->string('phone');
				$table->string('city');   
				$table->string('country');            
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('contacts');
		}
	}


php artisan migrate

php artisan make:controller ContactController --resource

# Modification du ContactController

    <?php
    
    namespace App\Http\Controllers;
    
    use Illuminate\Http\Request;
    use App\Contact;
    
    class ContactController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $contacts = Contact::all();
    
            return view('contacts.index', compact('contacts'));
        }
    
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('contacts.create');
        }
    
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $request->validate([
                'first_name'=>'required',
                'last_name'=>'required',
                'email'=>'required'
            ]);
    
            $contact = new Contact([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'city' => $request->get('city'),
                'country' => $request->get('country')
            ]);
            $contact->save();
            return redirect('/contacts')->with('success', 'Contact saved!');
        }
    
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }
    
        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $contact = Contact::find($id);
            return view('contacts.edit', compact('contact'));        
        }
    
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $request->validate([
                'first_name'=>'required',
                'last_name'=>'required',
                'email'=>'required'
            ]);
    
            $contact = Contact::find($id);
            $contact->first_name =  $request->get('first_name');
            $contact->last_name = $request->get('last_name');
            $contact->email = $request->get('email');
            $contact->phone = $request->get('phone');
            $contact->city = $request->get('city');
            $contact->country = $request->get('country');
            $contact->save();
    
            return redirect('/contacts')->with('success', 'Contact updated!');
        }
    
        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $contact = Contact::find($id);
            $contact->delete();
    
            return redirect('/contacts')->with('success', 'Contact deleted!');
        }
    }


# Ajout de la route dans le web.php

Route::resource('contacts', 'ContactController');

# Ajout de Contact.php

	<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class Contact extends Model
	{
		protected $fillable = [
			'first_name',
			'last_name',
			'email',
			'city',
			'country',
			'phone'       
		];
	}

php artisan make:controller ContactController --resource

# Modification du ContactController.php

	<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Contact;

	class ContactController extends Controller
	{
		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index()
		{
			$contacts = Contact::all();

			return view('contacts.index', compact('contacts'));
		}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function create()
		{
			return view('contacts.create');
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(Request $request)
		{
			$request->validate([
				'first_name'=>'required',
				'last_name'=>'required',
				'email'=>'required'
			]);

			$contact = new Contact([
				'first_name' => $request->get('first_name'),
				'last_name' => $request->get('last_name'),
				'email' => $request->get('email'),
				'phone' => $request->get('phone'),
				'city' => $request->get('city'),
				'country' => $request->get('country')
			]);
			$contact->save();
			return redirect('/contacts')->with('success', 'Contact saved!');
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function show($id)
		{
			//
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function edit($id)
		{
			$contact = Contact::find($id);
			return view('contacts.edit', compact('contact'));        
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function update(Request $request, $id)
		{
			$request->validate([
				'first_name'=>'required',
				'last_name'=>'required',
				'email'=>'required'
			]);

			$contact = Contact::find($id);
			$contact->first_name =  $request->get('first_name');
			$contact->last_name = $request->get('last_name');
			$contact->email = $request->get('email');
			$contact->phone = $request->get('phone');
			$contact->city = $request->get('city');
			$contact->country = $request->get('country');
			$contact->save();

			return redirect('/contacts')->with('success', 'Contact updated!');
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id)
		{
			$contact = Contact::find($id);
			$contact->delete();

			return redirect('/contacts')->with('success', 'Contact deleted!');
		}
	}


# Création du base.blade.php dans les views.

	<!DOCTYPE html>
	<html lang="en">
	<head>
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <title>TEST CRUD</title>
	  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
	</head>
	<body>
	  <div class="container">
		@yield('main')
	  </div>
	  <script src="{{ asset('js/app.js') }}" type="text/js"></script>
	</body>
	</html>


# Création du dossier contacts (ressources/view/contacts) et des blades

# Création du fichier create.blade.php

	@extends('base')

	@section('main')
	<div class="row">
	 <div class="col-sm-8 offset-sm-2">
		<h1 class="display-3">Add a contact</h1>
	  <div>
		@if ($errors->any())
		  <div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				  <li>{{ $error }}</li>
				@endforeach
			</ul>
		  </div><br />
		@endif
		  <form method="post" action="{{ route('contacts.store') }}">
			  @csrf
			  <div class="form-group">    
				  <label for="first_name">First Name:</label>
				  <input type="text" class="form-control" name="first_name"/>
			  </div>

			  <div class="form-group">
				  <label for="last_name">Last Name:</label>
				  <input type="text" class="form-control" name="last_name"/>
			  </div>

			  <div class="form-group">
				  <label for="email">Email:</label>
				  <input type="text" class="form-control" name="email"/>
			  </div>
			  <div class="form-group">
				  <label for="city">City:</label>
				  <input type="text" class="form-control" name="city"/>
			  </div>
			  <div class="form-group">
				  <label for="country">Country:</label>
				  <input type="text" class="form-control" name="country"/>
			  </div>
			  <div class="form-group">
				  <label for="phone">Phone Number:</label>
				  <input type="text" class="form-control" name="phone"/>
			  </div>                         
			  <button type="submit" class="btn btn-primary-outline">Add contact</button>
		  </form>
	  </div>
	</div>
	</div>
	@endsection

# Création de l'index (index.blade.php)

	@extends('base')

	@section('main')
	<div class="row">
	<div class="col-sm-12">
		<h1 class="display-3">Contacts</h1>    
	  <table class="table table-striped">
		<thead>
			<tr>
			  <td>ID</td>
			  <td>Name</td>
			  <td>Email</td>
			  <td>Phone Number</td>
			  <td>City</td>
			  <td>Phone Number</td>
			  <td colspan = 2>Actions</td>
			</tr>
		</thead>
		<tbody>
			@foreach($contacts as $contact)
			<tr>
				<td>{{$contact->id}}</td>
				<td>{{$contact->first_name}} {{$contact->last_name}}</td>
				<td>{{$contact->email}}</td>
				<td>{{$contact->phone}}</td>
				<td>{{$contact->city}}</td>
				<td>{{$contact->country}}</td>
				<td>
					<a href="{{ route('contacts.edit',$contact->id)}}" class="btn btn-primary">Edit</a>
				</td>
				<td>
					<form action="{{ route('contacts.destroy', $contact->id)}}" method="post">
					  @csrf
					  @method('DELETE')
					  <button class="btn btn-danger" type="submit">Delete</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	  </table>
	<div>
	</div>
	@endsection
	<div class="col-sm-12">

	  @if(session()->get('success'))
		<div class="alert alert-success">
		  {{ session()->get('success') }}  
		</div>
	  @endif
	</div>

	<div>
		<a style="margin: 19px;" href="{{ route('contacts.create')}}" class="btn btn-primary">New contact</a>
		</div>  